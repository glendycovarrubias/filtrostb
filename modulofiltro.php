<?php
	include 'conexion.php';

	/****************************  Llena los SELECT *********************************/
	/****************************** AÑO *********************************************/
	$query_anio 		= "SELECT DISTINCT ANIO FROM detreg_encabezado WHERE ANIO != ''";
	#Remplazar para scriptcase
	// sc_lookup(dataAnio,$query_anio);
	$resultadoAnio 		= mysqli_query($conexion, $query_anio) or die ( "Algo ha ido mal en la consulta a la base de datos"); //Esto no va en scriptcase
	/*******************************************************************************/


	/******************************** SEMANA ***************************************/
	$query_semana 		= "SELECT DISTINCT PERIODO FROM detreg_encabezado WHERE PERIODO != ''";
	#Remplazar para scriptcase
	// sc_lookup(dataSemana,$query_semana);	
	$resultadoSemana 	= mysqli_query($conexion, $query_semana) or die ( "Algo ha ido mal en la consulta a la base de datos");//Esto no va en scriptcase
	/******************************************************************************/

	/********************************* GRUPO *************************************/
	$query_grupo	= "SELECT DISTINCT GRUPO FROM detreg_encabezado WHERE GRUPO != ''";
	#Remplazar para scriptcase
	// sc_lookup(dataGrupo,$query_grupo);
	$resultadoGrupo	= mysqli_query($conexion, $query_grupo) or die ( "Algo ha ido mal en la consulta a la base de datos"); //Esto no va en scriptcase
	/******************************************************************************/

	/****************************** DEPARTAMENTO **********************************/
	$query_departamento 	= "SELECT DISTINCT DEPARTAMENTO_ID FROM detreg_encabezado WHERE DEPARTAMENTO_ID != ''";
	#Remplazar para scriptcase
	// sc_lookup(dataDepartamento,$query_departamento);
	$resultadoDepartamento 	= mysqli_query($conexion, $query_departamento) or die ( "Algo ha ido mal en la consulta a la base de datos");//Esto no va en scriptcase
	/******************************************************************************/
	/**************************** Fin  LLena los SELECT ***************************/


	/******************** Recibiendo Valores de los Filtros ****************************/
	if( isset($_POST['valorAnio']) ) {
		/* Recibimos los valores Filtrado */ 
	  	$valorAnio 			= $_POST['valorAnio'];
	  	$valorSemana 		= $_POST['valorSemana'];
	  	$valorGrupo 		= $_POST['valorGrupo'];
	  	$valorDepartamento 	= $_POST['valorDepartamento'];
	  	// echo $valorAnio;

	  	// $queryFiltros = "SELECT * FROM detreg_encabezado WHERE ANIO LIKE '%".$valorAnio."%' AND PERIODO LIKE '%".$valorSemana."%' AND GRUPO LIKE '%".$valorGrupo."%' AND DEPARTAMENTO_ID LIKE '%".$valorDepartamento."%'";
	  	//SELECT * FROM detreg_encabezado de INNER JOIN transporte_solicitud_programado tsp ON de.ID = tsp.ID WHERE de.ANIO LIKE ' 18%' AND de.PERIODO LIKE '%%' AND de.GRUPO LIKE '%%' AND de.DEPARTAMENTO_ID LIKE '%%'
	  	// $queryFiltros = "
	  	// 					SELECT * FROM detreg_encabezado de 
	  	// 					INNER JOIN transporte_solicitud_programado tsp 
	  	// 						ON de.ID = tsp.ID 
	  	// 					WHERE 
	  	// 						de.ANIO LIKE '%".$valorAnio."%'
	  	// 						AND de.PERIODO LIKE '%".$valorSemana."%' 
	  	// 						AND de.GRUPO LIKE '%".$valorGrupo."%' 
	  	// 						AND de.DEPARTAMENTO_ID LIKE '%".$valorDepartamento."%'
	  	// 				";
	  	
	  	$queryFiltros = "
	  						SELECT 
	  							de.ID AS IdE,
								de.PERIODO AS PeriodoE,
								de.ANIO AS AnioE, 
								de.EMPLEADO_ID AS EmpleadoE,
								de.DEPARTAMENTO_ID AS DepartamentoE,
								de.AREA_ID AS AreaE,
								de.TURNO_ID AS TurnoE,
								de.PUESTO_ID AS PuestoE,
								de.CENTRO_COSTOS_ID AS CentroCostosE,
								de.GRUPO AS GrupoE,
								de.DEPENDENCIA_ID AS DepenciaE,
								de.SALARIO_REAL AS SalarioE,
								de.SUCURSAL_ID AS SucursalE,
								de.DIR_IND AS DirE,
								de.FECHA_GENERACION AS FechaGneracionE,
								tsp.ID AS IdTsp,
								tsp.ANIO AS AnioTsp,
								tsp.PERIODO AS PeriodoTsp,
								tsp.TURNO_ID AS TurnoTsp,
								tsp.GRUPO AS GrupoTsp,
								tsp.EMPLEADO_ID AS EmpleadoTsp,
								tsp.DEPARTAMENTO_ID AS DepartamentoTsp,
								tsp.POBLACION_ID AS PoblacionTsp,
								tsp.LOCALIDAD AS LocalidadTsp,
								tsp.TIPO_DE_JORNADA AS tipoJornadaTsp,
								tsp.FECHA AS FechaTsp,
								tsp.TIPO_DE_PROGRAMACION AS TipoProgramacionTsp,
								tsp.APLICA_TRANSPORTE AS statusChecked	
	  						FROM detreg_encabezado de 
	  							INNER JOIN transporte_solicitud_programado tsp 
	  								ON de.ID = tsp.ID 
	  						WHERE 
	  							de.ANIO LIKE '%".$valorAnio."%' 
	  							AND de.PERIODO LIKE '%".$valorSemana."%' 
	  							AND de.GRUPO LIKE '%".$valorGrupo."%' 
	  							AND de.DEPARTAMENTO_ID LIKE '%".$valorDepartamento."%'";

	  	// echo $queryFiltros;
	  	$resultadoFiltro	= mysqli_query($conexion, $queryFiltros);
	  	// echo $resultadoFiltro;
	  	// echo "<pre>";
	  	// print_r($resultadoFiltro);
	  	// echo "</pre>";
	  	// $resultaFiltroTB = mysqli_fetch_array($resultadoFiltro);
	  	//  echo "<pre>";
	  	// print_r($resultaFiltroTB);
	  	//  echo "</pre>";


				
	?>
	<!-- data-url="http://issues.wenzhixin.net.cn/examples/bootstrap_table/data" -->
	<!-- data-pagination="true"
	data-side-pagination="server" 
					data-filter-control="true"
					data-height="300"
				    data-click-to-select="true"
-->
	<div id ="conttabladatos">
		<table id="table"
				    class="table-responsive"
					data-toggle="table"
					data-toolbar="#toolbar"
					data-show-export="true"
					data-search="true">
				    <thead>									
				    <tr>
				       <!--  <th data-field="state" data-checkbox="true"></th>
				        <th data-field="id" data-align="right" data-sortable="true">AÑO</th>
				        <th data-field="name" data-align="center" data-sortable="true">SEMANA</th>
				        <th data-field="price" data-sortable="true">GRUPO</th> -->
				        <th data-checkbox="true"></th>
				        <th data-align="center" data-sortable="true">ID</th>
				        <th data-align="center" data-sortable="true">PERIODO</th>
				        <th data-align="center" data-sortable="true">ANIO</th>
				        <th data-align="center" data-sortable="true">EMPLEADO_ID</th>
				        <th data-align="center" data-sortable="true">DEPARTAMENTO_ID</th>
				        <th data-align="center" data-sortable="true">AREA_ID</th>
				        <th data-align="center" data-sortable="true">TURNO_ID</th>
				        <th data-align="center" data-sortable="true">PUESTO_ID</th>
				        <th data-align="center" data-sortable="true">CENTRO_COSTOS_ID</th>
				        <th data-align="center" data-sortable="true">GRUPO</th>
				        <th data-align="center" data-sortable="true">DEPENDENCIA_ID</th>
				        <th data-align="center" data-sortable="true">SALARIO_REAL</th>
				        <th data-align="center" data-sortable="true">SUCURSAL_ID</th>
				        <th data-align="center" data-sortable="true">DIR_IND</th>
				        <th data-align="center" data-sortable="true">FECHA_GENERACION</th>
				        <th data-align="center" data-sortable="true">ID</th>
				        <th data-align="center" data-sortable="true">ANIO</th>
				        <th data-align="center" data-sortable="true">PERIODO</th>
				        <th data-align="center" data-sortable="true">TURNO_ID</th>
				        <th data-align="center" data-sortable="true">GRUPO</th>
				        <th data-align="center" data-sortable="true">EMPLEADO_ID</th>
				        <th data-align="center" data-sortable="true">DEPARTAMENTO_ID</th>
				        <th data-align="center" data-sortable="true">POBLACION_ID</th>
				        <th data-align="center" data-sortable="true">LOCALIDAD</th>
				        <th data-align="center" data-sortable="true">TIPO_DE_JORNADA</th>
				        <th data-align="center" data-sortable="true">FECHA</th>
				        <th data-align="center" data-sortable="true">TIPO_DE_PROGRAMACION</th>
				        <!-- <th data-field="idE" data-align="center" data-sortable="true">APLICA_TRANSPORTE</th> -->
				    </tr>
				    </thead>
				    <tbody>
						<?php 

				    			while ($resultaFiltroTB = mysqli_fetch_array($resultadoFiltro))
										{	
											
											// print_r($resultaFiltroTB);
											// echo "<pre>";
											// echo $resultaFiltroTB['ANIO'];
											// echo "</pre>";
											?>
											<tr id="<?php echo $resultaFiltroTB['IdE']; ?>" data-status="<?php echo $resultaFiltroTB['statusChecked'] ?>" class="trPrincipal">
												<td></td>
												<td><?php echo $resultaFiltroTB['IdE']; ?></td>
												<td><?php echo $resultaFiltroTB['PeriodoE']; ?></td>
												<td><?php echo $resultaFiltroTB['AnioE']; ?></td>
												<td><?php echo $resultaFiltroTB['EmpleadoE']; ?></td>
												<td><?php echo $resultaFiltroTB['DepartamentoE']; ?></td>
												<td><?php echo $resultaFiltroTB['AreaE']; ?></td>
												<td><?php echo $resultaFiltroTB['TurnoE']; ?></td>
												<td><?php echo $resultaFiltroTB['PuestoE']; ?></td>
												<td><?php echo $resultaFiltroTB['CentroCostosE']; ?></td>
												<td><?php echo $resultaFiltroTB['GrupoE']; ?></td>
												<td><?php echo $resultaFiltroTB['DepenciaE']; ?></td>
												<td><?php echo $resultaFiltroTB['SalarioE']; ?></td>
												<td><?php echo $resultaFiltroTB['SucursalE']; ?></td>
												<td><?php echo $resultaFiltroTB['DirE']; ?></td>
												<td><?php echo $resultaFiltroTB['FechaGneracionE']; ?></td>
												<td><?php echo $resultaFiltroTB['IdTsp']; ?></td>
												<td><?php echo $resultaFiltroTB['AnioTsp']; ?></td>
												<td><?php echo $resultaFiltroTB['PeriodoTsp']; ?></td>
												<td><?php echo $resultaFiltroTB['TurnoTsp']; ?></td>
												<td><?php echo $resultaFiltroTB['GrupoTsp']; ?></td>
												<td><?php echo $resultaFiltroTB['EmpleadoTsp']; ?></td>
												<td><?php echo $resultaFiltroTB['DepartamentoTsp']; ?></td>
												<td><?php echo $resultaFiltroTB['PoblacionTsp']; ?></td>
												<td><?php echo $resultaFiltroTB['LocalidadTsp']; ?></td>
												<td><?php echo $resultaFiltroTB['tipoJornadaTsp']; ?></td>
												<td><?php echo $resultaFiltroTB['FechaTsp']; ?></td>
												<td><?php echo $resultaFiltroTB['TipoProgramacionTsp']; ?></td>
											</tr>
											<?php
										}
						?>				
				    </tbody>
		</table>
	</div> 
	<?php 
	}
	/***********************************************************************************/

	/************************ Grabando Datos Seleccionados *****************************/
	/* Seleccionados */
	if( isset($_POST['arrayIdCheckboxSeleccionados']) ) {
		// exit;
		// echo "<pre>";
		// print_r($_POST['arrayIdCheckboxSeleccionados']);
		// echo "</pre>";

		$arraysID 			= $_POST['arrayIdCheckboxSeleccionados'];
		$fechaAplicacion 	= $_POST['fechaAplicacion'];
		$tipoJornada 		= $_POST['tipoJornada'];
		$tipoProgramacion 	= $_POST['tipoProgramacion'];

		foreach ($arraysID as $key => $value) {
			// print_r($value);
			// echo "hola numero : ".$value;
			$query_grabarCambios 	= 
									"
										UPDATE transporte_solicitud_programado 
										SET 
											TIPO_DE_JORNADA = '".$tipoJornada."',
											FECHA = '".$fechaAplicacion."',
											TIPO_DE_PROGRAMACION ='".$tipoProgramacion."',
											APLICA_TRANSPORTE = '1' 
										WHERE ID='".$value."'
									";
			// sc_lookup(dataDepartamento,$query_departamento); scriptcase
			$resultadoGrabarCambios 	= mysqli_query($conexion, $query_grabarCambios) or die ( "Algo ha ido mal en la consulta a la base de datos");			
		}
	}

	/* Deseleccionados */
	if(isset($_POST['arrayIdCheckboxDeseleccionados'])){

		$arraysID = $_POST['arrayIdCheckboxDeseleccionados'];
		// $fechaAplicacion 	= $_POST['fechaAplicacion'];
		// $tipoJornada 		= $_POST['tipoJornada'];
		// $tipoProgramacion 	= $_POST['tipoProgramacion'];


		foreach ($arraysID as $key => $value) {
			// print_r($value);
			// echo "hola numero : ".$value;
			$query_grabarCambios 	= 
									"
										UPDATE transporte_solicitud_programado 
										SET
											APLICA_TRANSPORTE ='0' 
										WHERE ID='".$value."'
									";
			// sc_lookup(dataDepartamento,$query_departamento); scriptcase
			$resultadoGrabarCambios 	= mysqli_query($conexion, $query_grabarCambios) or die ( "Algo ha ido mal en la consulta a la base de datos");			
		}		
	}
	/**********************************************************************************/

	mysqli_close( $conexion );
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Modulo Filtro Tabla</title>

	<!-- __________________________ Estilos CSS __________________________________ -->
	<!-- Libreria los estilos bootsrap -->
	<link rel="stylesheet" href="libs/bootstrap/dist/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css"> -->

	<!-- Libreria los estilos de selectpicker -->
	<link rel="stylesheet" href="libs/bootstrap-select/dist/css/bootstrap-select.min.css">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css"> -->

	<!-- Libreria del Filtro Date -->
	<link rel="stylesheet" href="libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

	<!-- Libreria para las tablas en boostrap -->
	<link rel="stylesheet" href="libs/bootstrap-table/dist/bootstrap-table.min.css">
	<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css"> -->
	
	<!-- Estilos del Proyecto -->
	<link rel="stylesheet" href="main.min.css">

	<!-- __________________________ Javascript __________________________________ -->

  	<!-- Libreria Jquery -->
  	<script src="libs/jQuery/dist/jquery.min.js"></script>
	<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script> -->

	<!-- Libreria del javascript de bootsrap -->
	<script src="libs/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script> -->

	<!-- Libreria para el javascript del selectpicker-->
	<script src="libs/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script> -->

	<!-- Libreria para el javascript del datepicker -->
	<script src="libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script> -->

	<!-- Libreria para el javascript de las tablas de boostrap -->
	<script src="libs/bootstrap-table/dist/bootstrap-table.min.js"></script>
	<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script> -->

	<!-- Exportar excel -->
	<script src="https://rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js"></script>	

	<!-- Javascript del proyecto -->
	<script src="main.min.js"></script>

	<style>
		.row {
		    margin-right: -15px;
		    margin-left: -15px;
		    margin-top: 1%;
		    margin-bottom: 3%;
		}
	</style>
</head>
<body>
	<div id="moduloFiltroTb" class="container-fluid">
		<div class="panel panel-primary">
	  		<div class="panel-heading">
	    		<h3 class="panel-title">Solicitud de Transporte</h3>
	  		</div>
	  		<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<form class="form-horizontal">
								<label class="form-group row col-md-12" id="indicacion">Filtrar para comenzar:</label>
								<!-- Filtro Año -->
								<div class="form-group row col-md-12">
								    <label for="selectFiltroAnio" class="col-sm-2 control-label">Año</label>
								    <div class="col-sm-10">
								      	<select class="selectpicker row" data-live-search="true" id="anio">
											<option disabled selected="true">Filtrar por Año</option>
									    	<?php 
												while ($anio = mysqli_fetch_array($resultadoAnio))
												{
													?>
														<option data-tokens="<?php echo $anio['ANIO'] ?>" value="<?php echo $anio['ANIO'] ?>"><?php echo $anio['ANIO'] ?></option>
													<?php
												}

												#Remplazar para scriptcase
												/*
												foreach({dataAnio} as $anio){
													?>
														<option data-tokens="<?php echo $anio[NECESITOVERCOMOLLEGAENSCRIPTCASE] ?>"><?php echo $anio[NECESITOVERCOMOLLEGAENSCRIPTCASE] ?></option>
													<?php
												}
												*/
									    	?>
										</select>
								    </div>
								</div>
								<!-- Filtro Semana -->
								<div class="form-group row col-md-12">
							    	<label for="selectFiltroSemana" class="col-sm-2 control-label">Semana</label>
								    <div class="col-sm-10">
								      	<select class="selectpicker row" data-live-search="true" id="semana">
											<option disabled selected="true">Filtrar por Semana</option>
									    	<?php 
												while ($semana = mysqli_fetch_array($resultadoSemana))
												{
													?>
														<option data-tokens="<?php echo $semana['PERIODO'] ?>" value="<?php echo $semana['PERIODO'] ?>"><?php echo $semana['PERIODO'] ?></option>
													<?php
												}

												#Remplazar para scriptcase
												/*
												foreach({dataSemana} as $semana){
													?>
														<option data-tokens="<?php echo $semana[NECESITOVERCOMOLLEGAENSCRIPTCASE] ?>"><?php echo $semana[NECESITOVERCOMOLLEGAENSCRIPTCASE] ?></option>
													<?php
												}
												*/
									    	?>
										</select>
								    </div>
								</div>
								<!-- Filtro Grupo -->
								<div class="form-group row col-md-12">
								    <label for="selectFiltroGrupo" class="col-sm-2 control-label">Grupo</label>
								    <div class="col-sm-10">
								        <select class="selectpicker row" data-live-search="true" id="grupo">
								        	<option disabled selected="true">Filtrar por Grupo</option>
									    	<?php 
												while ($grupo = mysqli_fetch_array($resultadoGrupo))
												{
													?>
														<option data-tokens="<?php echo $grupo['GRUPO'] ?>" value="<?php echo $grupo['GRUPO'] ?>"><?php echo $grupo['GRUPO'] ?></option>
													<?php
												}

												#Remplazar para scriptcase
												/*
												foreach({dataGrupo} as $grupo){
													?>
														<option data-tokens="<?php echo $grupo[NECESITOVERCOMOLLEGAENSCRIPTCASE] ?>"><?php echo $grupo[NECESITOVERCOMOLLEGAENSCRIPTCASE] ?></option>
													<?php
												}
												*/
									    	?>
										</select>
								    </div>
							  	</div>
								<!-- Filtro Departamento -->
								<div class="form-group row col-md-12">
								    <label for="inputDepartamento" class="col-sm-2 control-label">Departamento</label>
								    <div class="col-sm-10">
								        <select class="selectpicker row" data-live-search="true" id="departamento">
								        	<option disabled selected="true">Filtrar por Departamento</option>
									    	<?php 
												while ($departamento = mysqli_fetch_array($resultadoDepartamento))
												{
													?>
														<option data-tokens="<?php echo $departamento['DEPARTAMENTO_ID'] ?>" value="<?php echo $departamento['DEPARTAMENTO_ID'] ?>"><?php echo $departamento['DEPARTAMENTO_ID'] ?></option>
													<?php
												}

												#Remplazar para scriptcase
												/*
												foreach({dataDepartamento} as $departamento){
													?>
														<option data-tokens="<?php echo $departamento[NECESITOVERCOMOLLEGAENSCRIPTCASE] ?>"><?php echo $departamento[NECESITOVERCOMOLLEGAENSCRIPTCASE] ?></option>
													<?php
												}
												*/
									    	?>
										</select>
								    </div>
							  	</div>
							  
							    <!-- <button type="submit" class="btn btn-default">Sign in</button> -->
							</form>					
						</div>
<!-- 						<div class="row col-xs-12 col-md-8">
							<div class="btn-group" role="group" aria-label="...">
								<button type="button" class="btn btn-default" id="filtrar">Filtrar</button>
								<button type="button" class="btn btn-default" id="guardar">Guardar</button>
								<button type="button" class="btn btn-default" id="refrescar">Refrescar</button>
								<button type="button" class="btn btn-default" id="exportar">Exportar</button>
							</div>
						</div> -->
					</div>
					<div class="col-xs-12 col-md-6">

						<!-- <div class="input-group row col-xs-12 col-md-8">
						 	<span class="input-group-addon" id="folio">Folio</span>
							<input type="text" class="form-control" placeholder="00000" aria-describedby="Numero_de_Folio">
						</div>
 -->
 						<label for="" class="row">Completar antes de guardar:</label>
						<div class="input-group row col-xs-12 col-md-8">
							<span class="input-group-addon" id="lb-fechaAplicacion">Fecha de<br>Aplicación</span>
							<div class="input-group date">
								<input type="text" class="form-control" id="fechaAplicacion" disabled><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							</div>
						</div>				

						<div class="row">
							<select class="selectpicker input-group col-xs-12 col-md-8" title="Tipo de jornada" id="tipoJornada">
							 	<option data-tokens="D">Diurno</option>
								<option data-tokens="N">Nocturo</option>
							</select>					
						</div>

						<div class="row">
							<select class="selectpicker input-group col-xs-12 col-md-8" id="tipoProgramacion" title="Tipo de Programación">
							 	<option data-tokens="N">Normal</option>
								<option data-tokens="E">Extra</option>
							</select>					
						</div>				

	<!-- 					<div class="row">
							<div class="input-group col-xs-12 col-md-8">
						    	<div class="input-group">
						    		<span class="input-group-btn">
						    			<button type="button" class="btn btn-default" id="buscar">
						    				<i class="glyphicon glyphicon-search"></i>
						    			</button>
						    		</span>
						      		<input type="text" id="buscador" class="form-control" placeholder="Buscador" disabled>
						    	</div>
						  	</div>
						</div>	 -->

						<div class="row">
							<div class="btn-group" role="group" aria-label="...">
								<button type="button" class="btn btn-default" id="filtrar">Filtrar</button>
								<button type="button" class="btn btn-default" id="guardar">Guardar</button>
								<button type="button" class="btn btn-default" id="refrescar">Refrescar</button>
								<button type="button" class="btn btn-default" id="exportar">Exportar</button>
							</div>
						</div>		
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" id="conten_tb">
						<!-- <table id="table"
							data-toggle="table"
						    data-url="http://issues.wenzhixin.net.cn/examples/bootstrap_table/data"
							data-pagination="true"
							data-side-pagination="server"
							data-page-list="[5, 10, 20, 50, 100, 200]"
							data-search="false"
							data-filter-control="true"
							data-show-export="true"
							data-toolbar="#toolbar"
							data-height="300"
						    class="table-responsive">
						    <thead>
						    <tr>
						        <th data-field="state" data-checkbox="true"></th>
						        <th data-field="id" data-align="right" data-sortable="true">Item ID</th>
						        <th data-field="name" data-align="center" data-sortable="true">Item Name</th>
						        <th data-field="price" data-sortable="true">Item Price</th>
						    </tr>
						    </thead>
						</table> -->
					</div>
				</div>
			</div>
			<div class="panel-footer">Copyright © 2018 Empresatal Todos los derechos reservados</div>
		</div>
	</div>
</body>
</html>