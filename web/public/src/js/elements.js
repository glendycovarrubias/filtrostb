let elementosglobales = {};

(function() {
	/**
	 * Elementos registrados en el DOM
	 */
	this.elementos = function(){
		return{
			contenedores : {
				moduloInsertTb 	: '#modulo_Insert_Transporte',
				filtroTb 		: '#moduloFiltroTb',
				contenedorTabla : '#conten_tb'
			},
			selectores : {
				selectdate				: '.input-group.date',
				selectpicker 			: '.selectpicker',
				selectFechaAplicacion 	: '#fechaAplicacion',
				selectTipoProgramacion	: '#tipoProgram',
				selectFecha 			: '#fecha',
				selectAnio 				: '#anio',
				selectSemana 			: '#semana',
				selectGrupo 			: '#grupo',
				selectDepartamento 		: '#departamento',
				selectTipoJornada		: '#tipoJornada'
			},
			botones : {
				btnFiltrarInsert 	: '#btn_FiltrarDEncabezado',
				btnDarAlta			: '#btn_DarAltaSolicitud',
				btnFiltrar 			: '#filtrar',
				btnGuardar 			: '#guardar',
				btnRefrescar 		: '#refrescar',
				btnExportar 		: '#exportar',
				btnBuscar			: '#buscar'
			},
			inputs : {
				folio : '#folio'
			}
		};
	};
}).apply(elementosglobales);