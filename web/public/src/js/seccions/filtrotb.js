let filtroTb = {};

(function($, elementosglobales){

	let elementos 	= elementosglobales.elementos();

	// this.moduloFiltroTb = function(){
	// 	datepicker.initDatepicker(elementos.datepicker.inputDate);
	// };
	
	/**
	 * Funcionalidad que ayuda filtrar las combinaciones de los Select
	 * CLICK boton filtrar
	 *
	 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
	 * date 2018-03-21
	 * @version [1.0]
	 * @return  {[type]} [description]
	 */
	this.filtrar = function(){
		$(elementos.contenedores.filtroTb).on('click', elementos.botones.btnFiltrar, function(){
			let 
				valorAnio 			= $(elementos.selectores.selectAnio).val(),
				valorSemana 		= $(elementos.selectores.selectSemana).val(),
				valorGrupo 			= $(elementos.selectores.selectGrupo).val(),
				valorDepartamento 	= $(elementos.selectores.selectDepartamento).val()
			;
			var parametros = {
                "valorAnio" 		: valorAnio,
                "valorSemana" 		: valorSemana,
                "valorGrupo" 		: valorGrupo,
                "valorDepartamento" : valorDepartamento 
	        };
	        $.ajax({
	                data:  parametros,
	                url:   'modulofiltro.php',
	                type:  'post',
	                beforeSend: function () {
	                        $("#conten_tb").html("Procesando, espere por favor...");
	                },
	                success:  function (response) {
	                	// console.log(response);
	                        // $("#conten_tb").html(response);
	                        // $("#conten_tb").append('#table');
	                    // $(response).find('#table').html ();
	                    var x = $("<div/>").html(response).find('#conttabladatos').html();
	                    // console.log(x);
	                    $("#conten_tb").html(x);
	                    $('#table').bootstrapTable();
	                    filtroTb.checkbox();

	                    /* Cuando hay datos ya seleccionados */
	                    let obtenerValorCheckbox = $('.trPrincipal').length;

	                    for (var i = 0; i < obtenerValorCheckbox; i++) {
	                    	// console.log($('.trPrincipal')[i].id);
	                    	// console.log($('.trPrincipal')[i].attributes[3].value);
	                    	let idCheckbox = $('.trPrincipal')[i].id;
	                    	let statusCheckbox = $('.trPrincipal')[i].attributes[3].value;
	                    	if(statusCheckbox == '1'){
	                    		$('#'+idCheckbox+' td input').click();
	                    	}
	                    }


	                    //Desbloquear
	                    $('#fechaAplicacion').removeAttr('disabled');
	                    // $('#tipoJornada').removeAttr('disabled');
	                    // $('#tipoProgramacion').removeAttr('disabled');
	                    $('#buscador').removeAttr('disabled');

	                    // let recolectarValoresCheckbox = $('#table tr').find('trPrincipal');
	                    // //.prevObject.length
	                    // let totalCheckboxValores = recolectarValoresCheckbox.prevObject.length;
	                    // for (var i = 0; i < totalCheckboxValores; i++) {
	                    // 	let d = totalCheckboxValores[i]
	                    // 	console.log(d);
	                    // 	// let f = $('#table tr').find('trPrincipal').prevObject[1].attributes[d].value;
	                    // 	// if(f == "1"){
		                   //  // 	$('#1 td input').click();
		                   //  // }
	                    // }
	                    // let pruebacheckone = $('#table tbody tr')["0"].attributes[2].value;
	                    // if(pruebacheckone == "1"){
	                    // 	$('#1 td input').click();
	                    // }
	                }
	        });
		});
	};

	this.checkbox = function(){

		// let parametros = {
		// 	seleccionados 	: [],
		// 	deseleccionados : []
		// };
		// Comprobar cuando cambia un checkbox
			$('#table tr td input').on('change', function() {
				


				// let clickCheckbox = $(this).data('index');
				// let busquedaCheckbox = $('tr[data-index="'+clickCheckbox+'"]').attr('id');
				// console.log($(this).data('index'));
				// console.log($(this).after().find('td'));
				// console.log($(this).attr('id'));
				// console.log(busquedaCheckbox);
				
			    if ($(this).is(':checked') ) {
			    	// console.log($(this).addClass('seleccionado'));
			    // 	$('.prueba').attr('data-prueba','checkedPrueba');
			    // 	// $('.prueba').attr('data')
			    // 	// parametros.seleccionados.push(busquedaCheckbox);
			    // 	// console.log(busquedaCheckbox);
			    // 	// console.log($(this)["0"].dataset.index);
			    // 	// console.log($(this).val());
			    //     // console.log("Checkbox " + $(this).prop("id") +  " (" + $(this).val() + ") => Seleccionado");
			    } else {
			    	$(this).addClass('deseleccionado');
			    	// $('.deseleccionado').parent().parent().attr('id');
			    	// console.log($(this).removeAttr('class'));
			    // 	$('.prueba').removeAttr('data-prueba');
			    // 	// parametros.deseleccionados.push(busquedaCheckbox);
			    // 	// console.log(busquedaCheckbox);
			    // 	// console.log($(this)["0"].dataset.index);
			    // 	// console.log($(this).val());
			    //     // console.log("Checkbox " + $(this).prop("id") +  " (" + $(this).val() + ") => Deseleccionado");
			    }
			    // console.log(parametros);
			});

			
	};

	/**
	 * Funcionalidad que permite guardar los checkbox y realizar update o inserte de los datos
	 * CLICK boton guardar
	 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
	 * date 2018-03-21
	 * @version [1.0]
	 * @return  {[type]} [description]
	 */
	this.guardar = function(){
		$(elementos.contenedores.filtroTb).on('click', elementos.botones.btnGuardar, function(){
			console.log('Guardar');

			let parametros = {
				'arrayIdCheckboxSeleccionados' 		: [],
				'arrayIdCheckboxDeseleccionados' 	: [],
				'fechaAplicacion'					: $('#fechaAplicacion').val(),
				'tipoJornada'				   		: $('#tipoJornada').val(),
				'tipoProgramacion'					: $('#tipoProgramacion').val()
			};

			// Comprobar cuando se deseleciona un checkbox
			// $('input[type=checkbox]:checked').on('change', function() {
			//     console.log("Checkbox " + $(this).prop("id") +  " (" + $(this).val() + ") => Deseleccionado");
			// }); 

			// let checkboxSeleccionados = $('.selected')[4].id;
			let checkboxSelected = $('#table').find('tr.selected');
			// let checkboxDeselect = $('#table').find('tr');
			let totalCheckboxSeleccionados = checkboxSelected.length;

			 let checkboxDeselect = $('.deseleccionado').parent().parent();
			 let totalCheckboxDeseleccionados = checkboxDeselect.length;
			// let totalCheckboxDeseleccionados = checkboxDeselect.length;
			// let arrayIdCheckbox = [];
			for (var i = 0; i < totalCheckboxSeleccionados; i++) {
				let idSelectSeleccionados = checkboxSelected[i].id;
				// console.log(totalCheckboxSeleccionados[i]);
				// console.log(idSelectSeleccionados);
				// arrayIdCheckbox.push(idSelectSeleccionados);
				parametros.arrayIdCheckboxSeleccionados.push(idSelectSeleccionados);
			}

			//["0"].id
			// $('.deseleccionado').parent().parent().attr('id');
			for (var i = 0; i < totalCheckboxDeseleccionados; i++) {
				let idSelectDeseleccionados = checkboxDeselect[i].id;
				// console.log(x);
				// console.log($('.deseleccionado').parent().parent()[x].id)
			// 	let idSelectDeseleccionados = checkboxDeselect[i].id;
			// 	// console.log(totalCheckboxSeleccionados[i]);
			// 	// console.log(idSelectDeseleccionados);
			// 	// arrayIdCheckbox.push(idSelectDeseleccionados);
				parametros.arrayIdCheckboxDeseleccionados.push(idSelectDeseleccionados);
			}
			// console.log(arrayIdCheckbox);
	        $.ajax({
	                data:  parametros,
	                url:   'modulofiltro.php',
	                type:  'post',
	                beforeSend: function () {
	                        $("#conten_tb").html("Grabando...");
	                },
	                success:  function (response) {
	                	// console.log(response);
	                        // $("#conten_tb").html(response);
	                        // $("#conten_tb").append('#table');
	                    // $(response).find('#table').html ();
	                    // var x = $("<div/>").html(response).find('#conttabladatos').html();
	                    // // console.log(x);
	                    // $("#conten_tb").html(x);
	                    // $('#table').bootstrapTable();
	                }
	        });			
		});
	};

	/**
	 * Funcionalidad que permite resfrescar y mostrar cambios que aplicaron al guardar
	 *
	 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
	 * date 2018-03-21
	 * @version [1.0]
	 * @return  {[type]} [description]
	 */
	this.refrescar = function(){
		$(elementos.contenedores.filtroTb).on('click', elementos.botones.btnRefrescar, function(){
			console.log('refrescar');
			// $('#table').bootstrapTable('refresh');
			$('#conttabladatos').remove();

			/**/
			let 
				valorAnio 			= $(elementos.selectores.selectAnio).val(),
				valorSemana 		= $(elementos.selectores.selectSemana).val(),
				valorGrupo 			= $(elementos.selectores.selectGrupo).val(),
				valorDepartamento 	= $(elementos.selectores.selectDepartamento).val()
			;
			var parametros = {
                "valorAnio" 		: valorAnio,
                "valorSemana" 		: valorSemana,
                "valorGrupo" 		: valorGrupo,
                "valorDepartamento" : valorDepartamento 
	        };
	        $.ajax({
	                data:  parametros,
	                url:   'modulofiltro.php',
	                type:  'post',
	                beforeSend: function () {
	                        $("#conten_tb").html("Procesando, espere por favor...");
	                },
	                success:  function (response) {
	                	// console.log(response);
	                        // $("#conten_tb").html(response);
	                        // $("#conten_tb").append('#table');
	                    // $(response).find('#table').html ();
	                    var x = $("<div/>").html(response).find('#conttabladatos').html();
	                    // console.log(x);
	                    $("#conten_tb").html(x);
	                    $('#table').bootstrapTable();
	                    filtroTb.checkbox();

	                    /* Cuando hay datos ya seleccionados */
	                    let obtenerValorCheckbox = $('.trPrincipal').length;

	                    for (var i = 0; i < obtenerValorCheckbox; i++) {
	                    	// console.log($('.trPrincipal')[i].id);
	                    	// console.log($('.trPrincipal')[i].attributes[3].value);
	                    	let idCheckbox = $('.trPrincipal')[i].id;
	                    	let statusCheckbox = $('.trPrincipal')[i].attributes[3].value;
	                    	if(statusCheckbox == '1'){
	                    		$('#'+idCheckbox+' td input').click();
	                    	}
	                    }


	                    //Desbloquear
	                    $('#fechaAplicacion').removeAttr('disabled');
	                    // $('#tipoJornada').removeAttr('disabled');
	                    // $('#tipoProgramacion').removeAttr('disabled');
	                    // $('#buscador').removeAttr('disabled');

	                    // let recolectarValoresCheckbox = $('#table tr').find('trPrincipal');
	                    // //.prevObject.length
	                    // let totalCheckboxValores = recolectarValoresCheckbox.prevObject.length;
	                    // for (var i = 0; i < totalCheckboxValores; i++) {
	                    // 	let d = totalCheckboxValores[i]
	                    // 	console.log(d);
	                    // 	// let f = $('#table tr').find('trPrincipal').prevObject[1].attributes[d].value;
	                    // 	// if(f == "1"){
		                   //  // 	$('#1 td input').click();
		                   //  // }
	                    // }
	                    // let pruebacheckone = $('#table tbody tr')["0"].attributes[2].value;
	                    // if(pruebacheckone == "1"){
	                    // 	$('#1 td input').click();
	                    // }
	                }
	        });
			/**/
		});
	};

	/**
	 * Funcionalidad que permite la exportación a excel o pdf
	 *
	 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
	 * date 2018-03-21
	 * @version [1.0]
	 * @return  {[type]} [description]
	 */
	this.exportar = function(){
		$(elementos.contenedores.filtroTb).on('click', elementos.botones.btnExportar, function(){
			$('#table').tableExport({
	      		type: 'excel',
	      		escape: false
	    	});
		});
	};

	/**
	 * Funcionalidad que permite buscar un dato en la tabla
	 *
	 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
	 * date 2018-03-21
	 * @version [1.0]
	 * @return  {[type]} [description]
	 */
	this.buscar = function(){
		$(elementos.contenedores.filtroTb).on('click', elementos.botones.btnBuscar, function(){
			console.log('buscar');
		});
	};
}).apply(filtroTb, [jQuery, elementosglobales]);
