let moduloInsertTransporte = {}; //namespace

(function($, elementosglobales){
	let elementos 	= elementosglobales.elementos();

	/**
	 * Funcionalidad que ayuda a Filtrar y traer la tabla de Alta Solicitud
	 *
	 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
	 * date 2018-03-29
	 * @version [1.0]
	 * @return  {[type]} [description]
	 */
	this.filtrar = function(){
		$(elementos.contenedores.moduloInsertTb).on('click', elementos.botones.btnFiltrarInsert, function(){
			
			/**
			 * Tomamos los valores de los filtros
			 */
			let 
				valorAnio 			= $(elementos.selectores.selectAnio).val(),
				valorSemana 		= $(elementos.selectores.selectSemana).val(),
				valorGrupo 			= $(elementos.selectores.selectGrupo).val(),
				valorDepartamento 	= $(elementos.selectores.selectDepartamento).val()
			;

			/**
			 * Parametros para la consulta
			 * @type {Object}
			 */
			var parametros = {
                "valorAnio" 		: valorAnio,
                "valorSemana" 		: valorSemana,
                "valorGrupo" 		: valorGrupo,
                "valorDepartamento" : valorDepartamento 
	        };
	        /**
	         * Redireccionamiento a la pagina y tabla
	         *
	         * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
	         * date 2018-03-29
	         * @version [1.0]
	         * @param   {[type]} )        {	                                                          $("#conten_tb").html("Procesando, espere        por favor...");	                } [description]
	         * @param   {[type]} success:               function (response) {	                    var x                                 [description]
	         * @return  {[type]}          [description]
	         */
	        $.ajax({
	                data:  parametros,
	                url:   'moduloInsertTransporte.php',
	                type:  'post',
	                beforeSend: function () {
	                        $(elementos.contenedores.contenedorTabla).html("Procesando, espere por favor <i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i> ...");
	                },
	                success:  function (response) {
	                    var x = $("<div/>").html(response).find('#conttabladatos').html();
	                    $("#conten_tb").html(x);
	                    $('#table').bootstrapTable();
	                    moduloInsertTransporte.exportar();
	                }
	        });		
		});
	};

	/**
	 * Funcionalidad que permite guardar los checkbox y realizar el inserte de los datos
	 * CLICK boton Dar alta
	 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
	 * date 2018-03-21
	 * @version [1.0]
	 * @return  {[type]} [description]
	 */
	this.guardar = function(){
		$(elementos.contenedores.moduloInsertTb).on('click', elementos.botones.btnDarAlta, function(){

			let valorFechaDeAplicacion = $('#fechaAplicacion').val();

			let parametros = {
				'arrayIdCheckboxSeleccionados' 		: [],
				'fechaAplicacion'					: moduloInsertTransporte.convertirFormatoFecha(valorFechaDeAplicacion),
				'tipoJornada'				   		: $('#tipoJornada').val(),
				'tipoProgramacion'					: $('#tipoProgramacion').val()
			};


			// let checkboxSeleccionados = $('.selected')[4].id;
			let checkboxSelected = $('#table').find('tr.selected');
			// let checkboxDeselect = $('#table').find('tr');
			let totalCheckboxSeleccionados = checkboxSelected.length;

			for (var i = 0; i < totalCheckboxSeleccionados; i++) {
				let idSelectSeleccionados = checkboxSelected[i].id;
				// console.log(totalCheckboxSeleccionados[i]);
				// console.log(idSelectSeleccionados);
				// arrayIdCheckbox.push(idSelectSeleccionados);
				parametros.arrayIdCheckboxSeleccionados.push(idSelectSeleccionados);
			}

	        $.ajax({
	                data:  parametros,
	                url:   'moduloInsertTransporte.php',
	                type:  'post',
	                beforeSend: function () {
	                        $("#conten_tb").html("Grabando...");
	                },
	                success:  function (response) {
						console.log('Se a guardado exitosamente');
	                }
	        });			
		});
	};

	/**
	 * Funcionalidad que ayuda a la exportación de la tabla
	 * Opciones de Type : 'csv', 'tsv', 'txt', 'sql', 'json', 'xml', 'excel', 'doc', 'png' or 'pdf'
	 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
	 * date 2018-03-31
	 * @version [1.0]
	 * @return  {[type]} [description]
	 */
	this.exportar = function(){
		let $table = $('#table');
		$('#export').click(function () {
	  		$table.tableExport({
	      		type: 'excel',
	      		escape: false
	    	});
	  	});
	};


	/**
	 * Copia de una función que ayuda a convertir la Fecha del input de fecha
	 *
	 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
	 * date 2018-03-31
	 * @version [1.0]
	 * @param   {[type]} inputFormat [description]
	 * @return  {[type]}             [description]
	 */
	this.convertirFormatoFecha = function(inputFormat){
  		function pad(s){ 
  			return (s < 10) ? '0' + s : s; 
  		}
  		
  		let d = new Date(inputFormat);
  		return [d.getFullYear(), pad(d.getMonth()+1), pad(d.getDate())].join('-');
	}

}).apply(moduloInsertTransporte, [jQuery, elementosglobales]);