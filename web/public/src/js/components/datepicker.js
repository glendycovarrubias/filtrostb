let datepicker = {};

(function($, elementosglobales){

	/**
	 * Consultamos el archivo donde estan registrados los elementos del DOM
	 * Disponibles en este archivo para usarlo dentro de cualquier función
	 * @type {[type]}
	 */
	let elementos 	= elementosglobales.elementos();

	/**
	 * Funcionalidad que inicializa el plugin para datepicker
	 *
	 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
	 * date 2018-03-21
	 * @version [1.0]
	 * @return  {[type]} [description]
	 */
	this.initDatepicker = function(){
		$(elementos.selectores.selectdate).datepicker();
	};



	// let elementos 	= elementosglobales.elementos();

	/**
	 * Función que inicializa el Datepicker
	 *
	 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
	 * date 2018-03-19
	 * @version [1.0]
	 * @return  {[type]} [description]
	 */
	// this.initDatepicker = function(id){
	// 	$(id).datetimepicker();
	// };
}).apply(datepicker, [jQuery, elementosglobales]);