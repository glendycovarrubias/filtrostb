let selectpicker = {};

(function($, elementosglobales){
	/**
	 * Consultamos el archivo donde estan registrados los elementos del DOM
	 * Disponibles en este archivo para usarlo dentro de cualquier función
	 * @type {[type]}
	 */
	let elementos 	= elementosglobales.elementos();

	this.initSelectpicker = function(){
		$(elementos.selectores.selectpicker).selectpicker();
	};

}).apply(selectpicker, [jQuery, elementosglobales]);