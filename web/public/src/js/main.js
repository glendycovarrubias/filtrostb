$(document).ready(function(){	
	/**
	 * Inicializar plugins
	 */
	datepicker.initDatepicker();
	selectpicker.initSelectpicker();

	/**
	 * Modulo de Insertar Transporte
	 */
	moduloInsertTransporte.filtrar();
	moduloInsertTransporte.guardar();
	/**
	 * Modulo -----
	 */
	filtroTb.filtrar();
	filtroTb.guardar();
	filtroTb.refrescar();
	filtroTb.exportar();
	filtroTb.buscar();
});