<?php
	include 'conexion.php';

	/****************************  Llena los SELECT *********************************/
	/****************************** AÑO *********************************************/
	$query_anio 		= "SELECT DISTINCT ANIO FROM detreg_encabezado WHERE ANIO != ''";
	$resultadoAnio 		= mysqli_query($conexion, $query_anio) or die ( "Algo ha ido mal en la consulta a la base de datos");
	/*******************************************************************************/

	/******************************** SEMANA ***************************************/
	$query_semana 		= "SELECT DISTINCT PERIODO FROM detreg_encabezado WHERE PERIODO != ''";	
	$resultadoSemana 	= mysqli_query($conexion, $query_semana) or die ( "Algo ha ido mal en la consulta a la base de datos");
	/******************************************************************************/

	/********************************* GRUPO *************************************/
	$query_grupo	= "SELECT DISTINCT GRUPO FROM detreg_encabezado WHERE GRUPO != ''";
	$resultadoGrupo	= mysqli_query($conexion, $query_grupo) or die ( "Algo ha ido mal en la consulta a la base de datos");
	/******************************************************************************/

	/****************************** DEPARTAMENTO **********************************/
	$query_departamento 	= "SELECT DISTINCT DEPARTAMENTO_ID FROM detreg_encabezado WHERE DEPARTAMENTO_ID != ''";
	$resultadoDepartamento 	= mysqli_query($conexion, $query_departamento) or die ( "Algo ha ido mal en la consulta a la base de datos");
	/******************************************************************************/
	/**************************** Fin  LLena los SELECT ***************************/

	/**********************  Filtrar Tabla ************************************/
	/******************** Recibiendo Valores de los Filtros ****************************/
	if( isset($_POST['valorAnio']) || isset($_POST['valorSemana']) || isset($_POST['valorGrupo']) || isset($_POST['valorDepartamento']) ) {
		/* Recibimos los valores Filtrado */ 
	  	$valorAnio 			= $_POST['valorAnio'];
	  	$valorSemana 		= $_POST['valorSemana'];
	  	$valorGrupo 		= $_POST['valorGrupo'];
	  	$valorDepartamento 	= $_POST['valorDepartamento'];

	  	$queryFiltros = "
	  						SELECT 
	  							de.ID AS IdE,
								de.PERIODO AS PeriodoE,
								de.ANIO AS AnioE, 
								de.EMPLEADO_ID AS EmpleadoE,
								de.DEPARTAMENTO_ID AS DepartamentoE,
								de.AREA_ID AS AreaE,
								de.TURNO_ID AS TurnoE,
								de.PUESTO_ID AS PuestoE,
								de.CENTRO_COSTOS_ID AS CentroCostosE,
								de.GRUPO AS GrupoE,
								de.DEPENDENCIA_ID AS DepenciaE,
								de.SALARIO_REAL AS SalarioE,
								de.SUCURSAL_ID AS SucursalE,
								de.DIR_IND AS DirE,
								de.FECHA_GENERACION AS FechaGneracionE
	  						FROM detreg_encabezado AS de
	  						WHERE
	  							ID_WEEKHEADER NOT IN (SELECT ID_WEEKHEADER FROM transporte_solicitud_programado)
	  							AND de.ANIO LIKE '%".$valorAnio."%' 
	  							AND de.PERIODO LIKE '%".$valorSemana."%' 
	  							AND de.GRUPO LIKE '%".$valorGrupo."%' 
	  							AND de.DEPARTAMENTO_ID LIKE '%".$valorDepartamento."%'
	  					";
	  	$resultadoFiltro	= mysqli_query($conexion, $queryFiltros);	
	?>
	<div id ="conttabladatos">
		<!-- 
			Descartando propiedades (Quiero meter paginacion mas adelante)
			data-page-list="[5, 10, 20, 50, 100, 200]"
			data-side-pagination="server"
			data-pagination="true"
			// data-filter-control="true"
			//data-click-to-select="true"
			// vista detallada data-show-toggle="true"
			//data-show-refresh="false"
		-->
		<table id="table"
				    class="table-responsive"
					data-toggle="table"
					data-toolbar="#toolbar"
					data-show-export="true"
					data-search="true"
					>
				    <thead>									
				    <tr>
				        <th data-field="state" data-checkbox="true"></th>
				        <th data-field="ID" data-align="center" data-sortable="true">ID</th>
				        <th data-field="PERIODO" data-align="center" data-sortable="true">PERIODO</th>
				        <th data-field="ANIO" data-align="center" data-sortable="true">ANIO</th>
				        <th data-field="EMPLEADO_ID" data-align="center" data-sortable="true">EMPLEADO_ID</th>
				        <th data-field="DEPARTAMENTO_ID" data-align="center" data-sortable="true">DEPARTAMENTO_ID</th>
				        <th data-field="AREA_ID" data-align="center" data-sortable="true">AREA_ID</th>
				        <th data-field="TURNO_ID" data-align="center" data-sortable="true">TURNO_ID</th>
				        <th data-field="PUESTO_ID" data-align="center" data-sortable="true">PUESTO_ID</th>
				        <th data-field="CENTRO_COSTOS_ID" data-align="center" data-sortable="true">CENTRO_COSTOS_ID</th>
				        <th data-field="GRUPO" data-align="center" data-sortable="true">GRUPO</th>
				        <th data-field="DEPARTAMENTO_ID" data-align="center" data-sortable="true">DEPENDENCIA_ID</th>
				        <th data-field="SALARIO_REAL" data-align="center" data-sortable="true">SALARIO_REAL</th>
				        <th data-field="SUCURSAL_ID" data-align="center" data-sortable="true">SUCURSAL_ID</th>
				        <th data-field="DIR_IND" data-align="center" data-sortable="true">DIR_IND</th>
				        <th data-field="FECHA_GENERACION" data-align="center" data-sortable="true">FECHA_GENERACION</th>
				    </tr>
				    </thead>
				    <tbody>
						<?php 
				    			while ($resultaFiltroTB = mysqli_fetch_array($resultadoFiltro))
										{	
											?>
											<tr id="<?php echo $resultaFiltroTB['IdE']; ?>" class="trPrincipal">
												<td></td>
												<td data-valor="<?php echo $resultaFiltroTB['IdE']; ?>"><?php echo $resultaFiltroTB['IdE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['PeriodoE']; ?>"><?php echo $resultaFiltroTB['PeriodoE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['AnioE']; ?>"><?php echo $resultaFiltroTB['AnioE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['EmpleadoE']; ?>"><?php echo $resultaFiltroTB['EmpleadoE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['DepartamentoE']; ?>"><?php echo $resultaFiltroTB['DepartamentoE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['AreaE']; ?>"><?php echo $resultaFiltroTB['AreaE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['TurnoE']; ?>"><?php echo $resultaFiltroTB['TurnoE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['PuestoE']; ?>"><?php echo $resultaFiltroTB['PuestoE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['CentroCostosE']; ?>"><?php echo $resultaFiltroTB['CentroCostosE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['GrupoE']; ?>"><?php echo $resultaFiltroTB['GrupoE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['DepenciaE']; ?>"><?php echo $resultaFiltroTB['DepenciaE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['SalarioE']; ?>"><?php echo $resultaFiltroTB['SalarioE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['SucursalE']; ?>"><?php echo $resultaFiltroTB['SucursalE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['DirE']; ?>"><?php echo $resultaFiltroTB['DirE']; ?></td>
												<td data-valor="<?php echo $resultaFiltroTB['FechaGneracionE']; ?>"><?php echo $resultaFiltroTB['FechaGneracionE']; ?></td>
											</tr>
											<?php
										}
						?>				
				    </tbody>
		</table>
	</div> 
	<?php 
	}
	/**********************  Fin filtrar Tabla ********************************/

	/************************ Grabando Datos Seleccionados *****************************/
	/* Seleccionados */
	if( isset($_POST['arrayIdCheckboxSeleccionados']) || isset($_POST['fechaAplicacion']) || isset($_POST['tipoJornada']) || isset($_POST['tipoProgramacion']) ) {

		$arraysID 			= $_POST['arrayIdCheckboxSeleccionados'];
		$fechaAplicacion 	= $_POST['fechaAplicacion'];
		$tipoJornada 		= $_POST['tipoJornada'];
		$tipoProgramacion 	= $_POST['tipoProgramacion'];

		foreach ($arraysID as $key => $value) {
			
			$queryBuscarDatos = 
							"
								SELECT * FROM hrm.detreg_encabezado WHERE ID = ".$value.";
							"
						;

			$resultadoBuscarIDDatos 	= mysqli_query($conexion, $queryBuscarDatos) or die ( "Algo ha ido mal en la consulta a la base de datos");
								
			while ($dato = mysqli_fetch_array($resultadoBuscarIDDatos))
				{
					$query_grabarDatos 	= 
												"
													INSERT INTO transporte_solicitud_programado
													(
														ID_WEEKHEADER,
														ANIO,
														PERIODO,
														TURNO_ID,
														GRUPO,
														EMPLEADO_ID,
														DEPARTAMENTO_ID,
														POBLACION_ID,
														LOCALIDAD,
														TIPO_DE_JORNADA,
														FECHA,
														TIPO_DE_PROGRAMACION,
														SUCURSAL_ID,
														DIR_IND,
														DEPENDENCIA_ID,
														CENTRO_COSTOS_ID,
														AREA_ID,
														APLICA_TRANSPORTE
													)
													VALUE 
													(
														"."'".$dato['ID_WEEKHEADER']."'".",
														"."'".$dato['ANIO']."'".",
														"."'".$dato['PERIODO']."'".",
														"."'".$dato['TURNO_ID']."'".",
														"."'".$dato['GRUPO']."'".",
														"."'".$dato['EMPLEADO_ID']."'".",
														"."'".$dato['DEPARTAMENTO_ID']."'".",
														"."'".$dato['POBLACION_ID']."'".",
														"."'".$dato['LOCALIDAD']."'".",
														"."'".$tipoJornada."'".",
														"."'".$fechaAplicacion."'".",
														"."'".$tipoProgramacion."'".",
														"."'".$dato['SUCURSAL_ID']."'".",
														"."'".$dato['DIR_IND']."'".",
														"."'".$dato['DEPENDENCIA_ID']."'".",
														"."'".$dato['CENTRO_COSTOS_ID']."'".",
														"."'".$dato['AREA_ID']."'".",
														1
													);
									";
				$resultadoGrabarDatos 	= mysqli_query($conexion, $query_grabarDatos) or die ( "Algo ha ido mal en la consulta a la base de datos");
				}			
		}
	}
	/**********************************************************************************/

	mysqli_close( $conexion );
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Alta Solicitud de Transporte</title>

	<!-- __________________________ Estilos CSS __________________________________ -->
	<!-- Libreria los estilos bootsrap -->
	<link rel="stylesheet" href="libs/bootstrap/dist/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css"> -->

	<!-- Libreria los estilos de selectpicker -->
	<link rel="stylesheet" href="libs/bootstrap-select/dist/css/bootstrap-select.min.css">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css"> -->

	<!-- Libreria del Filtro Date -->
	<link rel="stylesheet" href="libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

	<!-- Libreria para las tablas en boostrap -->
	<link rel="stylesheet" href="libs/bootstrap-table/dist/bootstrap-table.min.css">
	<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css"> -->
	
	<!-- Estilos del Proyecto -->
	<link rel="stylesheet" href="main.min.css">

	<!-- __________________________ Javascript __________________________________ -->

  	<!-- Libreria Jquery -->
  	<script src="libs/jQuery/dist/jquery.min.js"></script>
	<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.js"></script> -->

	<!-- Libreria del javascript de bootsrap -->
	<script src="libs/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script> -->

	<!-- Libreria para el javascript del selectpicker-->
	<script src="libs/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script> -->

	<!-- Libreria para el javascript del datepicker -->
	<script src="libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script> -->

	<!-- Libreria para el javascript de las tablas de boostrap -->
	<script src="libs/bootstrap-table/dist/bootstrap-table.min.js"></script>
	<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script> -->

	<!-- Exportar excel -->
	<script src="https://rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js"></script>

	<!-- Javascript del proyecto -->
	<script src="main.min.js"></script>

	<style>
		.row {
		    margin-right: -15px;
		    margin-left: -15px;
		    margin-top: 1%;
		    margin-bottom: 3%;
		}
	</style>
</head>
<body>
	<div id="modulo_Insert_Transporte" class="container-fluid">
		<div class="panel panel-primary">
	  		<div class="panel-heading">
	    		<h3 class="panel-title">Dar de Alta Solicitud de Transporte</h3>
	  		</div>
	  		<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<form class="form-horizontal">
								<label class="form-group row col-md-12" id="indicacion">Filtrar para comenzar:</label>
								<!-- Filtro Año -->
								<div class="form-group row col-md-12">
								    <label for="selectFiltroAnio" class="col-sm-2 control-label">Año</label>
								    <div class="col-sm-10">
								      	<select class="selectpicker row" data-live-search="true" id="anio">
											<option disabled selected="true">Filtrar por Año</option>
									    	<?php 
												while ($anio = mysqli_fetch_array($resultadoAnio))
												{
													?>
														<option data-tokens="<?php echo $anio['ANIO'] ?>" value="<?php echo $anio['ANIO'] ?>"><?php echo $anio['ANIO'] ?></option>
													<?php
												}
									    	?>
										</select>
								    </div>
								</div>
								<!-- Filtro Semana -->
								<div class="form-group row col-md-12">
							    	<label for="selectFiltroSemana" class="col-sm-2 control-label">Semana</label>
								    <div class="col-sm-10">
								      	<select class="selectpicker row" data-live-search="true" id="semana">
											<option disabled selected="true">Filtrar por Semana</option>
									    	<?php 
												while ($semana = mysqli_fetch_array($resultadoSemana))
												{
													?>
														<option data-tokens="<?php echo $semana['PERIODO'] ?>" value="<?php echo $semana['PERIODO'] ?>"><?php echo $semana['PERIODO'] ?></option>
													<?php
												}
									    	?>
										</select>
								    </div>
								</div>
								<!-- Filtro Grupo -->
								<div class="form-group row col-md-12">
								    <label for="selectFiltroGrupo" class="col-sm-2 control-label">Grupo</label>
								    <div class="col-sm-10">
								        <select class="selectpicker row" data-live-search="true" id="grupo">
								        	<option disabled selected="true">Filtrar por Grupo</option>
									    	<?php 
												while ($grupo = mysqli_fetch_array($resultadoGrupo))
												{
													?>
														<option data-tokens="<?php echo $grupo['GRUPO'] ?>" value="<?php echo $grupo['GRUPO'] ?>"><?php echo $grupo['GRUPO'] ?></option>
													<?php
												}
									    	?>
										</select>
								    </div>
							  	</div>
								<!-- Filtro Departamento -->
								<div class="form-group row col-md-12">
								    <label for="inputDepartamento" class="col-sm-2 control-label">Departamento</label>
								    <div class="col-sm-10">
								        <select class="selectpicker row" data-live-search="true" id="departamento">
								        	<option disabled selected="true">Filtrar por Departamento</option>
									    	<?php 
												while ($departamento = mysqli_fetch_array($resultadoDepartamento))
												{
													?>
														<option data-tokens="<?php echo $departamento['DEPARTAMENTO_ID'] ?>" value="<?php echo $departamento['DEPARTAMENTO_ID'] ?>"><?php echo $departamento['DEPARTAMENTO_ID'] ?></option>
													<?php
												}
									    	?>
										</select>
								    </div>
							  	</div>
							</form>					
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
 						<label for="" class="row">Completar antes de guardar:</label>
						<div class="input-group row col-xs-12 col-md-8">
							<span class="input-group-addon" id="lb-fechaAplicacion">Fecha de<br>Aplicación</span>
							<div class="input-group date">
								<input type="text" class="form-control" id="fechaAplicacion"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							</div>
						</div>				

						<div class="row">
							<select class="selectpicker input-group col-xs-12 col-md-8" title="Tipo de jornada" id="tipoJornada">
							 	<option value="D">Diurno</option>
								<option value="N">Nocturo</option>
							</select>					
						</div>

						<div class="row">
							<select class="selectpicker input-group col-xs-12 col-md-8" id="tipoProgramacion" title="Tipo de Programación">
							 	<option value="N">Normal</option>
								<option value="E">Extra</option>
							</select>					
						</div>				
						<div class="row">
							<div class="btn-group" role="group" aria-label="...">
								<button type="button" class="btn btn-primary" id="btn_FiltrarDEncabezado">Filtrar</button>
								<button type="button" class="btn btn-success" id="btn_DarAltaSolicitud">Dar de Alta</button>
								<!-- <button type="button" class="btn btn-default" id="refrescar">refresh</button> -->
  								<button id="export" class="btn btn-default">Exportar</button>
							</div>
						</div>		
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" id="conten_tb">
					</div>
				</div>
			</div>
			<div class="panel-footer">Copyright © 2018 Empresatal Todos los derechos reservados</div>
		</div>
	</div>
</body>
</html>