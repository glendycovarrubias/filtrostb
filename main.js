/*!
* Este archivo es parte del proyecto filtrostb
* 
* Modulo de filtros y tablas
* 
* Modulo de filtrotb que hice para apoyar en un proyecto a mi papá
* 
* @copyright Copyright (c);
* 
* 
* Hecho con <3 por Ing. Glendy Covarrubias Tzab
* 
* 
* 
*/
'use strict';

var elementosglobales = {};

(function () {
	/**
  * Elementos registrados en el DOM
  */
	this.elementos = function () {
		return {
			contenedores: {
				moduloInsertTb: '#modulo_Insert_Transporte',
				filtroTb: '#moduloFiltroTb',
				contenedorTabla: '#conten_tb'
			},
			selectores: {
				selectdate: '.input-group.date',
				selectpicker: '.selectpicker',
				selectFechaAplicacion: '#fechaAplicacion',
				selectTipoProgramacion: '#tipoProgram',
				selectFecha: '#fecha',
				selectAnio: '#anio',
				selectSemana: '#semana',
				selectGrupo: '#grupo',
				selectDepartamento: '#departamento',
				selectTipoJornada: '#tipoJornada'
			},
			botones: {
				btnFiltrarInsert: '#btn_FiltrarDEncabezado',
				btnDarAlta: '#btn_DarAltaSolicitud',
				btnFiltrar: '#filtrar',
				btnGuardar: '#guardar',
				btnRefrescar: '#refrescar',
				btnExportar: '#exportar',
				btnBuscar: '#buscar'
			},
			inputs: {
				folio: '#folio'
			}
		};
	};
}).apply(elementosglobales);
"use strict";

var datepicker = {};

(function ($, elementosglobales) {

	/**
  * Consultamos el archivo donde estan registrados los elementos del DOM
  * Disponibles en este archivo para usarlo dentro de cualquier función
  * @type {[type]}
  */
	var elementos = elementosglobales.elementos();

	/**
  * Funcionalidad que inicializa el plugin para datepicker
  *
  * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
  * date 2018-03-21
  * @version [1.0]
  * @return  {[type]} [description]
  */
	this.initDatepicker = function () {
		$(elementos.selectores.selectdate).datepicker();
	};

	// let elementos 	= elementosglobales.elementos();

	/**
  * Función que inicializa el Datepicker
  *
  * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
  * date 2018-03-19
  * @version [1.0]
  * @return  {[type]} [description]
  */
	// this.initDatepicker = function(id){
	// 	$(id).datetimepicker();
	// };
}).apply(datepicker, [jQuery, elementosglobales]);
"use strict";

var moduloInsertTransporte = {}; //namespace

(function ($, elementosglobales) {
	var elementos = elementosglobales.elementos();

	/**
  * Funcionalidad que ayuda a Filtrar y traer la tabla de Alta Solicitud
  *
  * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
  * date 2018-03-29
  * @version [1.0]
  * @return  {[type]} [description]
  */
	this.filtrar = function () {
		$(elementos.contenedores.moduloInsertTb).on('click', elementos.botones.btnFiltrarInsert, function () {

			/**
    * Tomamos los valores de los filtros
    */
			var valorAnio = $(elementos.selectores.selectAnio).val(),
			    valorSemana = $(elementos.selectores.selectSemana).val(),
			    valorGrupo = $(elementos.selectores.selectGrupo).val(),
			    valorDepartamento = $(elementos.selectores.selectDepartamento).val();

			/**
    * Parametros para la consulta
    * @type {Object}
    */
			var parametros = {
				"valorAnio": valorAnio,
				"valorSemana": valorSemana,
				"valorGrupo": valorGrupo,
				"valorDepartamento": valorDepartamento
			};
			/**
    * Redireccionamiento a la pagina y tabla
    *
    * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
    * date 2018-03-29
    * @version [1.0]
    * @param   {[type]} )        {	                                                          $("#conten_tb").html("Procesando, espere        por favor...");	                } [description]
    * @param   {[type]} success:               function (response) {	                    var x                                 [description]
    * @return  {[type]}          [description]
    */
			$.ajax({
				data: parametros,
				url: 'moduloInsertTransporte.php',
				type: 'post',
				beforeSend: function beforeSend() {
					$(elementos.contenedores.contenedorTabla).html("Procesando, espere por favor <i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i> ...");
				},
				success: function success(response) {
					var x = $("<div/>").html(response).find('#conttabladatos').html();
					$("#conten_tb").html(x);
					$('#table').bootstrapTable();
					moduloInsertTransporte.exportar();
				}
			});
		});
	};

	/**
  * Funcionalidad que permite guardar los checkbox y realizar el inserte de los datos
  * CLICK boton Dar alta
  * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
  * date 2018-03-21
  * @version [1.0]
  * @return  {[type]} [description]
  */
	this.guardar = function () {
		$(elementos.contenedores.moduloInsertTb).on('click', elementos.botones.btnDarAlta, function () {

			var valorFechaDeAplicacion = $('#fechaAplicacion').val();

			var parametros = {
				'arrayIdCheckboxSeleccionados': [],
				'fechaAplicacion': moduloInsertTransporte.convertirFormatoFecha(valorFechaDeAplicacion),
				'tipoJornada': $('#tipoJornada').val(),
				'tipoProgramacion': $('#tipoProgramacion').val()
			};

			// let checkboxSeleccionados = $('.selected')[4].id;
			var checkboxSelected = $('#table').find('tr.selected');
			// let checkboxDeselect = $('#table').find('tr');
			var totalCheckboxSeleccionados = checkboxSelected.length;

			for (var i = 0; i < totalCheckboxSeleccionados; i++) {
				var idSelectSeleccionados = checkboxSelected[i].id;
				// console.log(totalCheckboxSeleccionados[i]);
				// console.log(idSelectSeleccionados);
				// arrayIdCheckbox.push(idSelectSeleccionados);
				parametros.arrayIdCheckboxSeleccionados.push(idSelectSeleccionados);
			}

			$.ajax({
				data: parametros,
				url: 'moduloInsertTransporte.php',
				type: 'post',
				beforeSend: function beforeSend() {
					$("#conten_tb").html("Grabando...");
				},
				success: function success(response) {
					console.log('Se a guardado exitosamente');
				}
			});
		});
	};

	/**
  * Funcionalidad que ayuda a la exportación de la tabla
  * Opciones de Type : 'csv', 'tsv', 'txt', 'sql', 'json', 'xml', 'excel', 'doc', 'png' or 'pdf'
  * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
  * date 2018-03-31
  * @version [1.0]
  * @return  {[type]} [description]
  */
	this.exportar = function () {
		var $table = $('#table');
		$('#export').click(function () {
			$table.tableExport({
				type: 'excel',
				escape: false
			});
		});
	};

	/**
  * Copia de una función que ayuda a convertir la Fecha del input de fecha
  *
  * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
  * date 2018-03-31
  * @version [1.0]
  * @param   {[type]} inputFormat [description]
  * @return  {[type]}             [description]
  */
	this.convertirFormatoFecha = function (inputFormat) {
		function pad(s) {
			return s < 10 ? '0' + s : s;
		}

		var d = new Date(inputFormat);
		return [d.getFullYear(), pad(d.getMonth() + 1), pad(d.getDate())].join('-');
	};
}).apply(moduloInsertTransporte, [jQuery, elementosglobales]);
"use strict";

var filtroTb = {};

(function ($, elementosglobales) {

	var elementos = elementosglobales.elementos();

	// this.moduloFiltroTb = function(){
	// 	datepicker.initDatepicker(elementos.datepicker.inputDate);
	// };

	/**
  * Funcionalidad que ayuda filtrar las combinaciones de los Select
  * CLICK boton filtrar
  *
  * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
  * date 2018-03-21
  * @version [1.0]
  * @return  {[type]} [description]
  */
	this.filtrar = function () {
		$(elementos.contenedores.filtroTb).on('click', elementos.botones.btnFiltrar, function () {
			var valorAnio = $(elementos.selectores.selectAnio).val(),
			    valorSemana = $(elementos.selectores.selectSemana).val(),
			    valorGrupo = $(elementos.selectores.selectGrupo).val(),
			    valorDepartamento = $(elementos.selectores.selectDepartamento).val();
			var parametros = {
				"valorAnio": valorAnio,
				"valorSemana": valorSemana,
				"valorGrupo": valorGrupo,
				"valorDepartamento": valorDepartamento
			};
			$.ajax({
				data: parametros,
				url: 'modulofiltro.php',
				type: 'post',
				beforeSend: function beforeSend() {
					$("#conten_tb").html("Procesando, espere por favor...");
				},
				success: function success(response) {
					// console.log(response);
					// $("#conten_tb").html(response);
					// $("#conten_tb").append('#table');
					// $(response).find('#table').html ();
					var x = $("<div/>").html(response).find('#conttabladatos').html();
					// console.log(x);
					$("#conten_tb").html(x);
					$('#table').bootstrapTable();
					filtroTb.checkbox();

					/* Cuando hay datos ya seleccionados */
					var obtenerValorCheckbox = $('.trPrincipal').length;

					for (var i = 0; i < obtenerValorCheckbox; i++) {
						// console.log($('.trPrincipal')[i].id);
						// console.log($('.trPrincipal')[i].attributes[3].value);
						var idCheckbox = $('.trPrincipal')[i].id;
						var statusCheckbox = $('.trPrincipal')[i].attributes[3].value;
						if (statusCheckbox == '1') {
							$('#' + idCheckbox + ' td input').click();
						}
					}

					//Desbloquear
					$('#fechaAplicacion').removeAttr('disabled');
					// $('#tipoJornada').removeAttr('disabled');
					// $('#tipoProgramacion').removeAttr('disabled');
					$('#buscador').removeAttr('disabled');

					// let recolectarValoresCheckbox = $('#table tr').find('trPrincipal');
					// //.prevObject.length
					// let totalCheckboxValores = recolectarValoresCheckbox.prevObject.length;
					// for (var i = 0; i < totalCheckboxValores; i++) {
					// 	let d = totalCheckboxValores[i]
					// 	console.log(d);
					// 	// let f = $('#table tr').find('trPrincipal').prevObject[1].attributes[d].value;
					// 	// if(f == "1"){
					//  // 	$('#1 td input').click();
					//  // }
					// }
					// let pruebacheckone = $('#table tbody tr')["0"].attributes[2].value;
					// if(pruebacheckone == "1"){
					// 	$('#1 td input').click();
					// }
				}
			});
		});
	};

	this.checkbox = function () {

		// let parametros = {
		// 	seleccionados 	: [],
		// 	deseleccionados : []
		// };
		// Comprobar cuando cambia un checkbox
		$('#table tr td input').on('change', function () {

			// let clickCheckbox = $(this).data('index');
			// let busquedaCheckbox = $('tr[data-index="'+clickCheckbox+'"]').attr('id');
			// console.log($(this).data('index'));
			// console.log($(this).after().find('td'));
			// console.log($(this).attr('id'));
			// console.log(busquedaCheckbox);

			if ($(this).is(':checked')) {
				// console.log($(this).addClass('seleccionado'));
				// 	$('.prueba').attr('data-prueba','checkedPrueba');
				// 	// $('.prueba').attr('data')
				// 	// parametros.seleccionados.push(busquedaCheckbox);
				// 	// console.log(busquedaCheckbox);
				// 	// console.log($(this)["0"].dataset.index);
				// 	// console.log($(this).val());
				//     // console.log("Checkbox " + $(this).prop("id") +  " (" + $(this).val() + ") => Seleccionado");
			} else {
				$(this).addClass('deseleccionado');
				// $('.deseleccionado').parent().parent().attr('id');
				// console.log($(this).removeAttr('class'));
				// 	$('.prueba').removeAttr('data-prueba');
				// 	// parametros.deseleccionados.push(busquedaCheckbox);
				// 	// console.log(busquedaCheckbox);
				// 	// console.log($(this)["0"].dataset.index);
				// 	// console.log($(this).val());
				//     // console.log("Checkbox " + $(this).prop("id") +  " (" + $(this).val() + ") => Deseleccionado");
			}
			// console.log(parametros);
		});
	};

	/**
  * Funcionalidad que permite guardar los checkbox y realizar update o inserte de los datos
  * CLICK boton guardar
  * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
  * date 2018-03-21
  * @version [1.0]
  * @return  {[type]} [description]
  */
	this.guardar = function () {
		$(elementos.contenedores.filtroTb).on('click', elementos.botones.btnGuardar, function () {
			console.log('Guardar');

			var parametros = {
				'arrayIdCheckboxSeleccionados': [],
				'arrayIdCheckboxDeseleccionados': [],
				'fechaAplicacion': $('#fechaAplicacion').val(),
				'tipoJornada': $('#tipoJornada').val(),
				'tipoProgramacion': $('#tipoProgramacion').val()
			};

			// Comprobar cuando se deseleciona un checkbox
			// $('input[type=checkbox]:checked').on('change', function() {
			//     console.log("Checkbox " + $(this).prop("id") +  " (" + $(this).val() + ") => Deseleccionado");
			// }); 

			// let checkboxSeleccionados = $('.selected')[4].id;
			var checkboxSelected = $('#table').find('tr.selected');
			// let checkboxDeselect = $('#table').find('tr');
			var totalCheckboxSeleccionados = checkboxSelected.length;

			var checkboxDeselect = $('.deseleccionado').parent().parent();
			var totalCheckboxDeseleccionados = checkboxDeselect.length;
			// let totalCheckboxDeseleccionados = checkboxDeselect.length;
			// let arrayIdCheckbox = [];
			for (var i = 0; i < totalCheckboxSeleccionados; i++) {
				var idSelectSeleccionados = checkboxSelected[i].id;
				// console.log(totalCheckboxSeleccionados[i]);
				// console.log(idSelectSeleccionados);
				// arrayIdCheckbox.push(idSelectSeleccionados);
				parametros.arrayIdCheckboxSeleccionados.push(idSelectSeleccionados);
			}

			//["0"].id
			// $('.deseleccionado').parent().parent().attr('id');
			for (var i = 0; i < totalCheckboxDeseleccionados; i++) {
				var idSelectDeseleccionados = checkboxDeselect[i].id;
				// console.log(x);
				// console.log($('.deseleccionado').parent().parent()[x].id)
				// 	let idSelectDeseleccionados = checkboxDeselect[i].id;
				// 	// console.log(totalCheckboxSeleccionados[i]);
				// 	// console.log(idSelectDeseleccionados);
				// 	// arrayIdCheckbox.push(idSelectDeseleccionados);
				parametros.arrayIdCheckboxDeseleccionados.push(idSelectDeseleccionados);
			}
			// console.log(arrayIdCheckbox);
			$.ajax({
				data: parametros,
				url: 'modulofiltro.php',
				type: 'post',
				beforeSend: function beforeSend() {
					$("#conten_tb").html("Grabando...");
				},
				success: function success(response) {
					// console.log(response);
					// $("#conten_tb").html(response);
					// $("#conten_tb").append('#table');
					// $(response).find('#table').html ();
					// var x = $("<div/>").html(response).find('#conttabladatos').html();
					// // console.log(x);
					// $("#conten_tb").html(x);
					// $('#table').bootstrapTable();
				}
			});
		});
	};

	/**
  * Funcionalidad que permite resfrescar y mostrar cambios que aplicaron al guardar
  *
  * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
  * date 2018-03-21
  * @version [1.0]
  * @return  {[type]} [description]
  */
	this.refrescar = function () {
		$(elementos.contenedores.filtroTb).on('click', elementos.botones.btnRefrescar, function () {
			console.log('refrescar');
			// $('#table').bootstrapTable('refresh');
			$('#conttabladatos').remove();

			/**/
			var valorAnio = $(elementos.selectores.selectAnio).val(),
			    valorSemana = $(elementos.selectores.selectSemana).val(),
			    valorGrupo = $(elementos.selectores.selectGrupo).val(),
			    valorDepartamento = $(elementos.selectores.selectDepartamento).val();
			var parametros = {
				"valorAnio": valorAnio,
				"valorSemana": valorSemana,
				"valorGrupo": valorGrupo,
				"valorDepartamento": valorDepartamento
			};
			$.ajax({
				data: parametros,
				url: 'modulofiltro.php',
				type: 'post',
				beforeSend: function beforeSend() {
					$("#conten_tb").html("Procesando, espere por favor...");
				},
				success: function success(response) {
					// console.log(response);
					// $("#conten_tb").html(response);
					// $("#conten_tb").append('#table');
					// $(response).find('#table').html ();
					var x = $("<div/>").html(response).find('#conttabladatos').html();
					// console.log(x);
					$("#conten_tb").html(x);
					$('#table').bootstrapTable();
					filtroTb.checkbox();

					/* Cuando hay datos ya seleccionados */
					var obtenerValorCheckbox = $('.trPrincipal').length;

					for (var i = 0; i < obtenerValorCheckbox; i++) {
						// console.log($('.trPrincipal')[i].id);
						// console.log($('.trPrincipal')[i].attributes[3].value);
						var idCheckbox = $('.trPrincipal')[i].id;
						var statusCheckbox = $('.trPrincipal')[i].attributes[3].value;
						if (statusCheckbox == '1') {
							$('#' + idCheckbox + ' td input').click();
						}
					}

					//Desbloquear
					$('#fechaAplicacion').removeAttr('disabled');
					// $('#tipoJornada').removeAttr('disabled');
					// $('#tipoProgramacion').removeAttr('disabled');
					// $('#buscador').removeAttr('disabled');

					// let recolectarValoresCheckbox = $('#table tr').find('trPrincipal');
					// //.prevObject.length
					// let totalCheckboxValores = recolectarValoresCheckbox.prevObject.length;
					// for (var i = 0; i < totalCheckboxValores; i++) {
					// 	let d = totalCheckboxValores[i]
					// 	console.log(d);
					// 	// let f = $('#table tr').find('trPrincipal').prevObject[1].attributes[d].value;
					// 	// if(f == "1"){
					//  // 	$('#1 td input').click();
					//  // }
					// }
					// let pruebacheckone = $('#table tbody tr')["0"].attributes[2].value;
					// if(pruebacheckone == "1"){
					// 	$('#1 td input').click();
					// }
				}
			});
			/**/
		});
	};

	/**
  * Funcionalidad que permite la exportación a excel o pdf
  *
  * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
  * date 2018-03-21
  * @version [1.0]
  * @return  {[type]} [description]
  */
	this.exportar = function () {
		$(elementos.contenedores.filtroTb).on('click', elementos.botones.btnExportar, function () {
			$('#table').tableExport({
				type: 'excel',
				escape: false
			});
		});
	};

	/**
  * Funcionalidad que permite buscar un dato en la tabla
  *
  * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
  * date 2018-03-21
  * @version [1.0]
  * @return  {[type]} [description]
  */
	this.buscar = function () {
		$(elementos.contenedores.filtroTb).on('click', elementos.botones.btnBuscar, function () {
			console.log('buscar');
		});
	};
}).apply(filtroTb, [jQuery, elementosglobales]);
"use strict";

$(document).ready(function () {
	/**
  * Inicializar plugins
  */
	datepicker.initDatepicker();
	selectpicker.initSelectpicker();

	/**
  * Modulo de Insertar Transporte
  */
	moduloInsertTransporte.filtrar();
	moduloInsertTransporte.guardar();
	/**
  * Modulo -----
  */
	filtroTb.filtrar();
	filtroTb.guardar();
	filtroTb.refrescar();
	filtroTb.exportar();
	filtroTb.buscar();
});
"use strict";

var selectpicker = {};

(function ($, elementosglobales) {
	/**
  * Consultamos el archivo donde estan registrados los elementos del DOM
  * Disponibles en este archivo para usarlo dentro de cualquier función
  * @type {[type]}
  */
	var elementos = elementosglobales.elementos();

	this.initSelectpicker = function () {
		$(elementos.selectores.selectpicker).selectpicker();
	};
}).apply(selectpicker, [jQuery, elementosglobales]);